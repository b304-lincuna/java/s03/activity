package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;

public class FactorialNumber {

        public static void main(String[] args){
            Scanner in = new Scanner(System.in);

            System.out.println("Input an integer whose factorial will be computed: ");
            try{
                int num = in.nextInt();

                int answer =((num==0)?0:1);
                int counter=1;

                if(num>=0) {

                    do {
                        answer *= counter;
                        counter++;
                    } while (num >= counter);

                    System.out.println("The factorial of " + num + " is " + answer);

                    answer = ((num==0)?0:1);

                    for (counter = 1; num >= counter; counter++) {
                        answer *= counter;
                    }

                    System.out.println("The factorial of " + num + " is " + answer);
                }else{
                    System.out.println("Invalid number! please input number positive number");
                }
            }catch (Exception e){
                System.out.println("Invalid input! please input a positive number.");
                e.printStackTrace();
            }

        }

}
